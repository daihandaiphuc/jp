const LIBRARY = [{
    text: 'あ',
    key: 'a'
},
{
    text: 'い',
    key: 'i'
},
{
    text: 'う',
    key: 'u'
},
{
    text: 'え',
    key: 'e'
},
{
    text: 'お',
    key: 'o'
},
// 2
{
    text: 'か',
    key: 'ka'
},
{
    text: 'き',
    key: 'ki'
},
{
    text: 'く',
    key: 'ku'
},
{
    text: 'け',
    key: 'ke'
},
{
    text: 'こ',
    key: 'ko'
},
// 3
   {
    text: 'さ',
    key: 'sa'
},
{
    text: 'し',
    key: 'shi'
},
{
    text: 'す',
    key: 'su'
},
{
    text: 'せ',
    key: 'se'
},
{
    text: 'そ',
    key: 'so'
},
 // ４
{
    text: 'た',
    key: 'ta'
},
{
    text: 'ち',
    key: 'chi'
},
{
    text: 'つ',
    key: 'tsu'
},
{
    text: 'て',
    key: 'te'
},
{
    text: 'と',
    key: 'to'
},
// 5
{
    text: 'な',
    key: 'na'
},
{
    text: 'に',
    key: 'ni'
},
{
    text: 'ぬ',
    key: 'nu'
},
{
    text: 'ね',
    key: 'ne'
},
{
    text: 'の',
    key: 'no'
},
// h
{
    text: 'は',
    key: 'ha'
},
{
    text: 'ひ',
    key: 'hi'
},
{
    text: 'ふ',
    key: 'fu'
},
{
    text: 'へ',
    key: 'he'
},
{
    text: 'ほ',
    key: 'ho'
},
// m
{
    text: 'ま',
    key: 'ma'
},
{
    text: 'み',
    key: 'mi'
},
{
    text: 'む',
    key: 'mu'
},
{
    text: 'め',
    key: 'me'
},
{
    text: 'も',
    key: 'mo'
},
// y
{
    text: 'や',
    key: 'ya'
},
{
    text: 'ゆ',
    key: 'yu'
},
{
    text: 'よ',
    key: 'yo'
},
// r
{
    text: 'ら',
    key: 'ra'
},
{
    text: 'り',
    key: 'ri'
},
{
    text: 'る',
    key: 'ru'
},
{
    text: 'れ',
    key: 're'
},
{
    text: 'ろ',
    key: 'ro'
},
// w
  {
    text: 'わ',
    key: 'wa'
},
{
    text: '',
    key: 'o'
},
 {
    text: 'ん',
    key: 'n'
},

]

export default LIBRARY